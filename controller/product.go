package controller

import (
	"fmt"
	"html/template"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"
	"vendor-management-system/models"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateProduct godoc
// @Summary CreateProduct endpoint is used by the admin role user to create a new product.
// @Description CreateProduct endpoint is used by the admin role user to create a new product
// @Router /api/v1/auth/product/create [post]
// @Tags product
// @Accept json
// @Produce json
// @Param name formData string true "name of the product"
func CreateProduct(c *gin.Context) {

	var existingProduct models.Product
	// claims := jwt.ExtractClaims(c)
	// user_email := claims["email"]

	id, _ := models.Rdb.HGet("user", "ID").Result()
	ID, _ := strconv.Atoi(id)
	fmt.Println(ID)
	roleId, _ := models.Rdb.HGet("user", "RoleID").Result()
	var User models.User
	
	if roleId == "" {
		fmt.Println("Redis empty....checking Database for user...")
		err := IsCheckRedis(c)
		if err != nil {
			c.JSON(404, gin.H{
				"error": "something went wrong with redis",
			})
			return
		}
	}

	roleId, _ = models.Rdb.HGet("user", "RoleID").Result()
	// Check if the current user had admin role.
	if roleId != "1" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product can only be added by admin"})
		return
	}
	// if err := models.DB.Where("email = ? AND user_role_id=1", user_email).First(&User).Error; err != nil {
	// 	c.JSON(http.StatusBadRequest, gin.H{"error": "Product can only be added by admin user"})
	// 	return
	// }

	c.Request.ParseForm()

	if c.PostForm("title") == "" {
		ReturnParameterMissingError(c, "title")
		
	}
	
	title := template.HTMLEscapeString(c.PostForm("title"))
	price, err := strconv.Atoi(template.HTMLEscapeString(c.PostForm("price")))
	sale_price, _ := strconv.Atoi(template.HTMLEscapeString(c.PostForm("sale_price")))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "can only convert string to int",
		})
	}

	//Check if the product already exists.
	err = models.DB.Where("title = ?").First(&existingProduct).Error
	if err == nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "product already exists."})
		return
	}

	product := models.Product{
		Title:      title,
		Price:      price,
		Sale_Price: sale_price,
		// Available: available,
		CreatedBy:  User.ID,
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	}

	err = models.DB.Create(&product).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusCreated, gin.H{
		"id":   product.ID,
		"title": product.Title,
		"price": product.Price,
		"sale_price": product.Sale_Price,
	})

}

// UpdateProduct godoc
// @Summary UpdateProduct endpoint is used by the admin role user to update a new product.
// @Description Updateproduct endpoint is used by the admin role user to update a new product
// @Router /api/v1/auth/products/:id/ [PATCH]
// @Tags product
// @Accept json
// @Produce json
func UpdateProduct(c *gin.Context) {
	var existingProduct models.Product
	var updateProduct models.Product
	// claims := jwt.ExtractClaims(c)

	// user_email, _ := claims["email"]
	//var User models.User
	// user_email, _ := Rdb.HGet("user", "email").Result()

	id, _ := models.Rdb.HGet("user", "RoleID").Result()
	if id == "" {
		fmt.Println("Redis empty....checking Database for user...")
		err := IsCheckRedis(c)
		if err != nil {
			c.JSON(404, gin.H{
				"error": "something went wrong with redis",
			})
			return
		}
	}
	id, _ = models.Rdb.HGet("user", "RoleID").Result()
	// c.JSON(http.StatusCreated, gin.H{"msg": "Success"})

	if id != "1" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product can only be updated by admin"})
		return
	}

	// Check if the product already exists.
	err := models.DB.Where("id = ?", c.Param("id")).First(&existingProduct).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "product does not exists."})
		return
	}

	if err := c.ShouldBindJSON(&updateProduct); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	models.DB.Model(&existingProduct).Updates(updateProduct)

}

type ReturnedProduct struct {
	ID         int    `json:"id"`
	Title      string `json:"name"`
	Price      int    `json:"price"`
	Sale_Price       int `json:"sale_price"`
}

// GetProduct godoc
// @Summary GetP endpoint is used to get info of a product..
// @Description GetProduct endpoint is used to get info of a product.
// @Router /api/v1/auth/product/:id/ [get]
// @Tags product
// @Accept json
// @Produce json
func GetProduct(c *gin.Context) {
	var existingProduct models.Product
	var images []models.ProductImage
	user_email, _ := models.Rdb.HGet("user", "email").Result()
	// Check if the product already exists.
	fmt.Println("user" + user_email)

	err := models.DB.Where("id = ?", c.Param("id")).First(&existingProduct).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "product does not exists."})
		return
	}

	err = models.DB.Where("product_id = ?", c.Param("id")).First(&images).Error
	if err != nil {
		c.JSON(404, gin.H{"error": err})
	}

	id, _ := models.Rdb.HGet("user", "RoleID").Result()
	if id != "1" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Products can only be seen by admin"})
		return
	}
	// GET FROM CACHE FIRST
	c.JSON(http.StatusOK, gin.H{
		"book": existingProduct,
		"images":  images,
	})

}

// ListAllProduct godoc
// @Summary ListAllProduct endpoint is used to list all product.
// @Description API Endpoint to register the user with the role of Supervisor or Admin.
// @Router /api/v1/auth/product/ [get]
// @Tags product
// @Accept json
// @Produce json
func ListAllProduct(c *gin.Context) {

	var Product []models.Product
	var existingProduct []ReturnedProduct
	email := c.GetString("user_email")
	fmt.Println("c variable" + email)
	email, _ = models.Rdb.HGet("user", "email").Result()

	if email == "" {
		fmt.Println("Redis empty....checking Database for user...")
		err := IsCheckRedis(c)
		if err != nil {
			c.JSON(404, gin.H{
				"error": "something went wrong with redis",
			})
			return
		}
	}
	id, _ := models.Rdb.HGet("user", "RoleID").Result()

	
	if id != "1" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product can only be seen by admin"})
		return
	}

	models.DB.Model(Product).Find(&existingProduct)
	c.JSON(http.StatusOK, existingProduct)

}


// func FilterProduct(c *gin.Context) {
// 	var product []models.Product
// 	var existingProduct []ReturnedProduct
//     sort.Slice(product, func(i, j int) bool {

// 	Sale_Price := product[i].Sale_Price > product[j].Sale_Price

// 	if product[i].Sale_Price != product[j].Sale_Price {
// 		return product[i].Sale_Price < product[j].Sale_Price
// 	}
// 	return product[i].Sale_Price > product[j].Sale_Price
// }
// return Sale_Price
// })

// c.JSON(http.StatusOK, existingProduct)

// }
// DeleteProduct godoc
// @Summary DeleteProduct endpoint is used to delete a product.
// @Description DeleteProduct endpoint is used to delete a product.
// @Router /api/v1/auth/product/delete/:id/ [delete]
// @Tags product
// @Accept json
// @Produce json
// @Success 200 {object} models.Product
// @Failure 400,404 {object} object
func DeleteProduct(c *gin.Context) {
	var existingProduct models.Product
	
	// claims := jwt.ExtractClaims(c)
	// user_email := claims["email"]
	

	id, _ := models.Rdb.HGet("user", "RoleID").Result()
	if id == "" {
		fmt.Println("Redis empty....checking Database for user...")
		err := IsCheckRedis(c)
		if err != nil {
			c.JSON(404, gin.H{
				"error": "something went wrong with redis",
			})
			return
		}
	}

	id, _ = models.Rdb.HGet("user", "RoleID").Result()
	if id != "1" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product can only be deleted by admin"})
		return
	}
	// Check if the current user had admin role.
	// if err := models.DB.Where("email = ? AND user_role_id=1", user_email).First(&User).Error; err != nil {
	// 	c.JSON(http.StatusBadRequest, gin.H{"error": "Product can only be deleted by admin user"})
	// 	return
	// }

		// Check if the product already exists.
		err := models.DB.Where("id = ?", c.Param("id")).First(&existingProduct).Error
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "product does not exists."})
			return
		}
		models.DB.Where("id = ?", c.Param("id")).Delete(&existingProduct)
		// GET FROM CACHE FIRST
		c.JSON(http.StatusOK, gin.H{"Success": "Product deleted"})
}




type UploadedFile struct {
	Status   bool
	ProductId   int
	Filename string
	Path     string
	Err      string
}

func generateFilePath(id string, extension string) string {
	// Generate random file name for the new uploaded file so it doesn't override the old file with same name
	newFileName := uuid.New().String() + extension

	fmt.Println(newFileName)
	projectFolder, err := os.Getwd()
	// projectFolder, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	localS3Folder := filepath.ToSlash(projectFolder) + "/locals3/"
	productImageFolder := localS3Folder + id + "/"

	fmt.Println(productImageFolder)

	if _, err := os.Stat(productImageFolder); err != nil {
		os.MkdirAll(productImageFolder, os.ModeDir)
		fmt.Println("Uploaded Image to folder")
	}

	imagePath := productImageFolder + newFileName
	return imagePath
}

func SaveToBucket(c *gin.Context, f *multipart.FileHeader, extension string, filename string) UploadedFile {
	/*
		whitelist doctionary for extensions
		golang doesnot support "for i in x" construct like python,
		Iterating the list would be expensive, thus we need to use a struct to prevent for loop.
	*/
	acceptedExtensions := map[string]bool{
		".png":  true,
		".jpg":  true,
		".JPEG": true,
		".PNG":  true,
	}
	id, _ := strconv.Atoi(c.Param("id"))

	if !acceptedExtensions[extension] {
		return UploadedFile{Status: false, ProductId: id, Filename: filename, Err: "Invalid Extension"}
	}

	filePath := generateFilePath(c.Param("id"), extension)
	fmt.Println(filePath)
	err := c.SaveUploadedFile(f, filePath)

	if err == nil {
		return UploadedFile{
			Status:   true,
			ProductId:   id,
			Filename: filename,
			Path:     filePath,
			Err:      "",
		}
	}
	return UploadedFile{Status: false, ProductId: id, Filename: filename, Err: ""}
}

func UploadProductImages(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))

	if !IsAdmin(c) {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Image can only be added by admin"})
		return
	}

	if !DoesProductAvailable(id) {
		c.JSON(http.StatusNotFound, "Product does not exist")
		return
	}

	form, _ := c.MultipartForm()
	files := form.File["file"]

	var SuccessfullyUploadedFiles []UploadedFile
	var UnSuccessfullyUploadedFiles []UploadedFile
	var ProductImages []models.ProductImage

	for _, f := range files {
		//save the file to specific dst
		extension := filepath.Ext(f.Filename)
		fmt.Println(extension)
		uploaded_file := SaveToBucket(c, f, extension, f.Filename)
		if uploaded_file.Status {
			SuccessfullyUploadedFiles = append(SuccessfullyUploadedFiles, uploaded_file)
			ProductImages = append(ProductImages, models.ProductImage{
				Image_Path:       uploaded_file.Path,
				ProductId:    uploaded_file.ProductId,
				
			})

		} else {
			UnSuccessfullyUploadedFiles = append(UnSuccessfullyUploadedFiles, uploaded_file)
		}
	}
	models.DB.Create(&ProductImages)

	c.JSON(http.StatusOK, gin.H{
		"successful": SuccessfullyUploadedFiles, "unsuccessful": UnSuccessfullyUploadedFiles,
	})

}