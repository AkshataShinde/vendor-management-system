package routes

import (
	"vendor-management-system/controller"

	middlewares "vendor-management-system/middleware"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

func PublicEndpoints(r *gin.RouterGroup, authMiddleware *jwt.GinJWTMiddleware) {
	// Generate public endpoints - [ signup] - api/v1/signup

	r.POST("/signup", controller.Signup)
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.POST("/login", authMiddleware.LoginHandler)
	r.POST("/logout", authMiddleware.LogoutHandler)
}

func AuthenticatedEndpoints(r *gin.RouterGroup, authMiddleware *jwt.GinJWTMiddleware) {
	// Generate Authenticated endpoints - [] - api/v1/auth/
	r.Use(authMiddleware.MiddlewareFunc())

	r.POST("vendor/create", controller.CreateVendor)
	r.POST("admin/create", controller.CreateAdmin)

	//product endpoints
	r.POST("product/create", controller.CreateProduct)
	r.GET("product/", controller.ListAllProduct)
	r.GET("product/:id", controller.GetProduct)
	r.PATCH("product/:id", controller.UpdateProduct)
	// r.GET("product/filter", controller.FilterProduct)
	r.POST("product/:id/image/upload", controller.UploadProductImages)
	r.DELETE("product/delete/:id", controller.DeleteProduct)

	// //order endpoints
	// r.GET("order/", controller.ListAllOrder)
	// r.POST("order/create", controller.CreateOrder)
	// r.PATCH("order/:id", controller.UpdateOrder)
	// r.GET("order/:id", controller.GetOrder)

}

func GetRouter(router chan *gin.Engine) {
	gin.ForceConsoleColor()
	r := gin.Default()

	r.Use(cors.Default())

	authMiddleware, _ := middlewares.GetAuthMiddleware()

	// Create a BASE_URL - /api/v1
	v1 := r.Group("/api/v1/")
	PublicEndpoints(v1, authMiddleware)
	AuthenticatedEndpoints(v1.Group("auth"), authMiddleware)
	router <- r
}
