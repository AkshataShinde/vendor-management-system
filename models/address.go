package models

type Address struct {
    Id int `json:"id"`
	Address_line1 string `json:"address_line1"`
	Adress_line2 string `json:"address_line2"`
	Pincode int `json:"pincode"`
	City string `json:"city"`
	State_ID int `json:"state_id"`
    Vendor_ID int `json:"vendor_id"`
}