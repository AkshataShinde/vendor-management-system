package models

import (
	"time"
)

type UserRole struct {
	Id   int    `json:"id"`
	Role string `json:"role"`
}

type User struct {
	ID                 int    `json:"id"`
	Email              string `json:"email" gorm:"unique"`
	Mobile             string `json:"mobile"`
	FirstName          string `json:"first_name"`
	LastName           string `json:"last_name"`
	Password           string `json:"password"`
	UserRoleID         int    `json:"user_role_id"`
	Primary_address_id string    `json:"primary_address"`

	IsVerified bool `json:"is_verified"`

	// Meta
	Created_by int       `json:"created_by"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
}