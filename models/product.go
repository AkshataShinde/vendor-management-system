package models

import (
	"time"
)

type Product struct {
	ID         int    `json:"id"`
	Title       string  `json:"title"`
	Description string  `json:"description"`
	Available   bool    `json:"available"`
	Price       int `json:"price"`
	Status      bool    `json:"status"`
	Sale_Price  int `json:"sale_price"`

	CreatedAt  time.Time `json:"created_at"`
	CreatedBy int       `json:"created_by"`
	UpdatedAt  time.Time `json:"updated_at"`
}

type ProductImage struct {
	ID         int    `json:"id"`
	Image_Path string `json:"image_path"`
	ProductId  int
	Product    Product `gorm:"foreignKey:ProductId"`
}