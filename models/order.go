package models

import (
	"time"
)

type Order struct {
	Order_id          int       `gorm:"autoIncrement"`
	Vendor_id         int       `gorm:"autoIncrement"`
	Order_status      bool      `json:"order_status"`
	Orderdate_time    time.Time `json:"orderdate_time"`
	Isdelivered       bool      `json:"isdelivered"`
	Deliverydate_time time.Time `json:"deliverydate_time"`
	Order_product_id  int       `json:"order_product_id"`
}

type OrderProducts struct {
	Product         string  `json:"product"`
	Price           float64 `json:"price"`
	Is_sale_product bool    `json:"is_sale_product"`
	Order_id        int     `json:"order_id"`
}